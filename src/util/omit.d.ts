declare type DynamicValues = import("./InterfaceParameters").DynamicValues;
export declare function omit<T extends DynamicValues>(data: T, check: (propertyName: keyof T, propertyValue: T[keyof T]) => boolean): T;
export declare function mergeAllPrototypes<T extends DynamicValues>(data: T): T;
export declare function omitEmptyCheck<T extends DynamicValues>(property: keyof T, value: T[keyof T]): boolean;
export declare function omitEmpty<T extends DynamicValues>(data: T): Partial<T>;
export declare function omitFunctions<T extends DynamicValues>(data: T): { [key in { [K in keyof T]: T[K] extends Function ? never : K; }[keyof T]]: T[key]; };
export {};
//# sourceMappingURL=omit.d.ts.map