export default function wait<T extends [...any[]]>(ms: number, ...args: T): Promise<T>;
//# sourceMappingURL=wait.d.ts.map