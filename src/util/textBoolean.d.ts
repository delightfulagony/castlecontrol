/**
 * Input some text that you expect to have a yes or no answer.
 * If the text does not match any of the filters, it will throw an error.
 *
 * `new Error("Cannot determine a valid answer");`
 * @param text the string to get a boolean from
 */
export default function textBoolean(text: string): boolean;
//# sourceMappingURL=textBoolean.d.ts.map