declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
export interface exportChatInviteLink_options {
    /**
     * Unique identifier for the target chat or username of the target channel (in the format @channelusername)
     */
    chat_id: Integer | string;
}
/**
 * Use this method to generate a new invite link for a chat; any previously generated link is revoked. The bot must be an administrator in the chat for this to work and must have the appropriate admin rights. Returns the new invite link as string on success.
 */
export declare type exportChatInviteLink = (options: exportChatInviteLink_options) => Promise<Result<string>>;
export {};
//# sourceMappingURL=exportChatInviteLink.d.ts.map