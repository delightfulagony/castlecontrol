declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
import { ForceReply } from "../types/ForceReply";
import { InlineKeyboardMarkup } from "../types/InlineKeyboardMarkup";
import { Message } from "../types/Message";
import { ReplyKeyboardMarkup } from "../types/ReplyKeyboardMarkup";
import { ReplyKeyboardRemove } from "../types/ReplyKeyboardRemove";
export interface sendMessage_options {
    /**
     * Unique identifier for the target chat or username of the target channel (in the format @channelusername)
     */
    chat_id: Integer | string;
    /**
     * Text of the message to be sent
     */
    text: string;
    /**
     * Send Markdown or HTML, if you want Telegram apps to show bold, italic, fixed-width text or inline URLs in your bot's message.
     */
    parse_mode?: string;
    /**
     * Disables link previews for links in this message
     */
    disable_web_page_preview?: boolean;
    /**
     * Sends the message silently. Users will receive a notification with no sound.
     */
    disable_notification?: boolean;
    /**
     * If the message is a reply, ID of the original message
     */
    reply_to_message_id?: Integer;
    /**
     * Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
     */
    reply_markup?: InlineKeyboardMarkup | ReplyKeyboardMarkup | ReplyKeyboardRemove | ForceReply;
}
/**
 * Use this method to send text messages. On success, the sent Message is returned.
 */
export declare type sendMessage = (options: sendMessage_options) => Promise<Result<Message>>;
export {};
//# sourceMappingURL=sendMessage.d.ts.map