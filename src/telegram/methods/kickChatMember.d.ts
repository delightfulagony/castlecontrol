declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
export interface kickChatMember_options {
    /**
     * Unique identifier for the target group or username of the target supergroup or channel (in the format @channelusername)
     */
    chat_id: Integer | string;
    /**
     * Unique identifier of the target user
     */
    user_id: Integer;
    /**
     * Date when the user will be unbanned, unix time. If user is banned for more than 366 days or less than 30 seconds from the current time they are considered to be banned forever
     */
    until_date?: Integer;
}
/**
 * Use this method to kick a user from a group, a supergroup or a channel. In the case of supergroups and channels, the user will not be able to return to the group on their own using invite links, etc., unless unbanned first. The bot must be an administrator in the chat for this to work and must have the appropriate admin rights. Returns True on success.
 */
export declare type kickChatMember = (options: kickChatMember_options) => Promise<Result<boolean>>;
export {};
//# sourceMappingURL=kickChatMember.d.ts.map