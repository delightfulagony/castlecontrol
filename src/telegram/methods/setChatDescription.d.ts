declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
export interface setChatDescription_options {
    /**
     * Unique identifier for the target chat or username of the target channel (in the format @channelusername)
     */
    chat_id: Integer | string;
    /**
     * New chat description, 0-255 characters
     */
    description?: string;
}
/**
 * Use this method to change the description of a supergroup or a channel. The bot must be an administrator in the chat for this to work and must have the appropriate admin rights. Returns True on success.
 */
export declare type setChatDescription = (options: setChatDescription_options) => Promise<Result<boolean>>;
export {};
//# sourceMappingURL=setChatDescription.d.ts.map