import { Result } from "../../bot/TelegramBot";
import { InlineKeyboardMarkup } from "../types/InlineKeyboardMarkup";
import { Message } from "../types/Message";
export interface editMessageCaption_options {
    /**
     * Required if inline_message_id is not specified. Unique identifier for the target chat or username of the target channel (in the format @channelusername)
     */
    chat_id?: Integer | string;
    /**
     * Required if inline_message_id is not specified. Identifier of the sent message
     */
    message_id?: Integer;
    /**
     * Required if chat_id and message_id are not specified. Identifier of the inline message
     */
    inline_message_id?: string;
    /**
     * New caption of the message
     */
    caption?: string;
    /**
     * Send Markdown or HTML, if you want Telegram apps to show bold, italic, fixed-width text or inline URLs in the media caption.
     */
    parse_mode?: string;
    /**
     * A JSON-serialized object for an inline keyboard.
     */
    reply_markup?: InlineKeyboardMarkup;
}
/**
 * Use this method to edit captions of messages sent by the bot or via the bot (for inline bots). On success, if edited message is sent by the bot, the edited Message is returned, otherwise True is returned.
 */
export declare type editMessageCaption = (options: editMessageCaption_options) => Promise<Result<Message | True>>;
//# sourceMappingURL=editMessageCaption.d.ts.map