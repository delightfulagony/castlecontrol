declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
export interface sendChatAction_options {
    /**
     * Unique identifier for the target chat or username of the target channel (in the format @channelusername)
     */
    chat_id: Integer | string;
    /**
     * Type of action to broadcast. Choose one, depending on what the user is about to receive: typing for text messages, upload_photo for photos, record_video or upload_video for videos, record_audio or upload_audio for audio files, upload_document for general files, find_location for location data, record_video_note or upload_video_note for video notes.
     */
    action: string;
}
/**
 * Use this method when you need to tell the user that something is happening on the bot's side. The status is set for 5 seconds or less (when a message arrives from your bot, Telegram clients clear its typing status). Returns True on success.
 */
export declare type sendChatAction = (options: sendChatAction_options) => Promise<Result<boolean>>;
export {};
//# sourceMappingURL=sendChatAction.d.ts.map