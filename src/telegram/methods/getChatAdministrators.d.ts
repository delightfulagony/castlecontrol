declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
import { ChatMember } from "../types/ChatMember";
export interface getChatAdministrators_options {
    /**
     * Unique identifier for the target chat or username of the target supergroup or channel (in the format @channelusername)
     */
    chat_id: Integer | string;
}
/**
 * Use this method to get a list of administrators in a chat. On success, returns an Array of ChatMember objects that contains information about all chat administrators except other bots. If the chat is a group or a supergroup and no administrators were appointed, only the creator will be returned.
 */
export declare type getChatAdministrators = (options: getChatAdministrators_options) => Promise<Result<ChatMember[]>>;
export {};
//# sourceMappingURL=getChatAdministrators.d.ts.map