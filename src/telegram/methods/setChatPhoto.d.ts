declare type Integer = number;
import { Result } from "../../bot/TelegramBot";
import { InputFile } from "../types/InputFile";
export interface setChatPhoto_options {
    /**
     * Unique identifier for the target chat or username of the target channel (in the format @channelusername)
     */
    chat_id: Integer | string;
    /**
     * New chat photo, uploaded using multipart/form-data
     */
    photo: InputFile;
}
/**
 * Use this method to set a new profile photo for the chat. Photos can't be changed for private chats. The bot must be an administrator in the chat for this to work and must have the appropriate admin rights. Returns True on success.
 */
export declare type setChatPhoto = (options: setChatPhoto_options) => Promise<Result<boolean>>;
export {};
//# sourceMappingURL=setChatPhoto.d.ts.map