/**
 * A placeholder, currently holds no information. Use BotFather to set up your game.
 */
export interface CallbackGame {
}
export declare class CallbackGame implements CallbackGame {
    constructor(data: CallbackGame);
}
//# sourceMappingURL=CallbackGame.d.ts.map