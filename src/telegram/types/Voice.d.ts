declare type Integer = number;
/**
 * This object represents a voice note.
 */
export interface Voice {
    /**
     * Unique identifier for this file
     */
    file_id: string;
    /**
     * Duration of the audio in seconds as defined by sender
     */
    duration: Integer;
    /**
     * Optional. MIME type of the file as defined by sender
     */
    mime_type?: string;
    /**
     * Optional. File size
     */
    file_size?: Integer;
}
export declare class Voice implements Voice {
    /**
     * Unique identifier for this file
     */
    file_id: string;
    /**
     * Duration of the audio in seconds as defined by sender
     */
    duration: Integer;
    /**
     * Optional. MIME type of the file as defined by sender
     */
    mime_type?: string;
    /**
     * Optional. File size
     */
    file_size?: Integer;
    constructor(data: Voice);
}
export {};
//# sourceMappingURL=Voice.d.ts.map