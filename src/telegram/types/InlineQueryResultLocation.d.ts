declare type Float = number;
declare type Integer = number;
import { InlineKeyboardMarkup } from "./InlineKeyboardMarkup";
import { InputMessageContent } from "./InputMessageContent";
/**
 * Represents a location on a map. By default, the location will be sent by the user. Alternatively, you can use input_message_content to send a message with the specified content instead of the location.
 */
export interface InlineQueryResultLocation {
    /**
     * Type of the result, must be location
     */
    type: string;
    /**
     * Unique identifier for this result, 1-64 Bytes
     */
    id: string;
    /**
     * Location latitude in degrees
     */
    latitude: Float;
    /**
     * Location longitude in degrees
     */
    longitude: Float;
    /**
     * Location title
     */
    title: string;
    /**
     * Optional. Period in seconds for which the location can be updated, should be between 60 and 86400.
     */
    live_period?: Integer;
    /**
     * Optional. Inline keyboard attached to the message
     */
    reply_markup?: InlineKeyboardMarkup;
    /**
     * Optional. Content of the message to be sent instead of the location
     */
    input_message_content?: InputMessageContent;
    /**
     * Optional. Url of the thumbnail for the result
     */
    thumb_url?: string;
    /**
     * Optional. Thumbnail width
     */
    thumb_width?: Integer;
    /**
     * Optional. Thumbnail height
     */
    thumb_height?: Integer;
}
export declare class InlineQueryResultLocation implements InlineQueryResultLocation {
    constructor(data: InlineQueryResultLocation);
}
export {};
//# sourceMappingURL=InlineQueryResultLocation.d.ts.map