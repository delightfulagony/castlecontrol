declare type Float = number;
/**
 * This object represents a point on the map.
 */
export interface Location {
    /**
     * Longitude as defined by sender
     */
    longitude: Float;
    /**
     * Latitude as defined by sender
     */
    latitude: Float;
}
export declare class Location implements Location {
    constructor(data: Location);
}
export {};
//# sourceMappingURL=Location.d.ts.map