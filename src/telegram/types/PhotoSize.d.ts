declare type Integer = number;
/**
 * This object represents one size of a photo or a file / sticker thumbnail.
 */
export interface PhotoSize {
    /**
     * Unique identifier for this file
     */
    file_id: string;
    /**
     * Photo width
     */
    width: Integer;
    /**
     * Photo height
     */
    height: Integer;
    /**
     * Optional. File size
     */
    file_size?: Integer;
}
export declare class PhotoSize implements PhotoSize {
    constructor(data: PhotoSize);
}
export {};
//# sourceMappingURL=PhotoSize.d.ts.map