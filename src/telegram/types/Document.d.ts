declare type Integer = number;
import { PhotoSize } from "./PhotoSize";
/**
 * This object represents a general file (as opposed to photos, voice messages and audio files).
 */
export interface Document {
    /**
     * Unique file identifier
     */
    file_id: string;
    /**
     * Optional. Document thumbnail as defined by sender
     */
    thumb?: PhotoSize;
    /**
     * Optional. Original filename as defined by sender
     */
    file_name?: string;
    /**
     * Optional. MIME type of the file as defined by sender
     */
    mime_type?: string;
    /**
     * Optional. File size
     */
    file_size?: Integer;
}
export declare class Document implements Document {
    constructor(data: Document);
}
export {};
//# sourceMappingURL=Document.d.ts.map