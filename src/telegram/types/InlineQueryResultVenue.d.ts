declare type Float = number;
declare type Integer = number;
import { InlineKeyboardMarkup } from "./InlineKeyboardMarkup";
import { InputMessageContent } from "./InputMessageContent";
/**
 * Represents a venue. By default, the venue will be sent by the user. Alternatively, you can use input_message_content to send a message with the specified content instead of the venue.
 */
export interface InlineQueryResultVenue {
    /**
     * Type of the result, must be venue
     */
    type: string;
    /**
     * Unique identifier for this result, 1-64 Bytes
     */
    id: string;
    /**
     * Latitude of the venue location in degrees
     */
    latitude: Float;
    /**
     * Longitude of the venue location in degrees
     */
    longitude: Float;
    /**
     * Title of the venue
     */
    title: string;
    /**
     * Address of the venue
     */
    address: string;
    /**
     * Optional. Foursquare identifier of the venue if known
     */
    foursquare_id?: string;
    /**
     * Optional. Foursquare type of the venue, if known. (For example, “arts_entertainment/default”, “arts_entertainment/aquarium” or “food/icecream”.)
     */
    foursquare_type?: string;
    /**
     * Optional. Inline keyboard attached to the message
     */
    reply_markup?: InlineKeyboardMarkup;
    /**
     * Optional. Content of the message to be sent instead of the venue
     */
    input_message_content?: InputMessageContent;
    /**
     * Optional. Url of the thumbnail for the result
     */
    thumb_url?: string;
    /**
     * Optional. Thumbnail width
     */
    thumb_width?: Integer;
    /**
     * Optional. Thumbnail height
     */
    thumb_height?: Integer;
}
export declare class InlineQueryResultVenue implements InlineQueryResultVenue {
    constructor(data: InlineQueryResultVenue);
}
export {};
//# sourceMappingURL=InlineQueryResultVenue.d.ts.map