declare type Integer = number;
/**
 * This object represents a phone contact.
 */
export interface Contact {
    /**
     * Contact's phone number
     */
    phone_number: string;
    /**
     * Contact's first name
     */
    first_name: string;
    /**
     * Optional. Contact's last name
     */
    last_name?: string;
    /**
     * Optional. Contact's user identifier in Telegram
     */
    user_id?: Integer;
    /**
     * Optional. Additional data about the contact in the form of a vCard
     */
    vcard?: string;
}
export declare class Contact implements Contact {
    constructor(data: Contact);
}
export {};
//# sourceMappingURL=Contact.d.ts.map