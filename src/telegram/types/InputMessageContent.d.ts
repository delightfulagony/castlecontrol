import { InputContactMessageContent } from "./InputContactMessageContent";
import { InputLocationMessageContent } from "./InputLocationMessageContent";
import { InputTextMessageContent } from "./InputTextMessageContent";
import { InputVenueMessageContent } from "./InputVenueMessageContent";
/**
 * This object represents the content of a message to be sent as a result of an inline query. Telegram clients currently support the following 4 types
 */
export declare type InputMessageContent = InputTextMessageContent | InputLocationMessageContent | InputVenueMessageContent | InputContactMessageContent;
export declare function InputMessageContent<T extends InputMessageContent = InputMessageContent>(data: T): T;
//# sourceMappingURL=InputMessageContent.d.ts.map