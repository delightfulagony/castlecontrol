declare type Integer = number;
import { MaskPosition } from "./MaskPosition";
import { PhotoSize } from "./PhotoSize";
/**
 * This object represents a sticker.
 */
export interface Sticker {
    /**
     * Unique identifier for this file
     */
    file_id: string;
    /**
     * Sticker width
     */
    width: Integer;
    /**
     * Sticker height
     */
    height: Integer;
    /**
     * Optional. Sticker thumbnail in the .webp or .jpg format
     */
    thumb?: PhotoSize;
    /**
     * Optional. Emoji associated with the sticker
     */
    emoji?: string;
    /**
     * Optional. Name of the sticker set to which the sticker belongs
     */
    set_name?: string;
    /**
     * Optional. For mask stickers, the position where the mask should be placed
     */
    mask_position?: MaskPosition;
    /**
     * Optional. File size
     */
    file_size?: Integer;
}
export declare class Sticker implements Sticker {
    constructor(data: Sticker);
}
export {};
//# sourceMappingURL=Sticker.d.ts.map