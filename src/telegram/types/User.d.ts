declare type Integer = number;
/**
 * This object represents a Telegram user or bot.
 */
export interface User {
    /**
     * Unique identifier for this user or bot
     */
    id: Integer;
    /**
     * True, if this user is a bot
     */
    is_bot: boolean;
    /**
     * User‘s or bot’s first name
     */
    first_name: string;
    /**
     * Optional. User‘s or bot’s last name
     */
    last_name?: string | null;
    /**
     * Optional. User‘s or bot’s username
     */
    username?: string | null;
    /**
     * Optional. IETF language tag of the user's language
     */
    language_code?: string | null;
}
export declare class User implements User {
    constructor(data: User);
}
export declare function isUser(user: any): user is User;
export declare function getUsersInObject(obj: any): User[];
export {};
//# sourceMappingURL=User.d.ts.map