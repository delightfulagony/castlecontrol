import { PassportElementErrorDataField } from "./PassportElementErrorDataField";
import { PassportElementErrorFile } from "./PassportElementErrorFile";
import { PassportElementErrorFiles } from "./PassportElementErrorFiles";
import { PassportElementErrorFrontSide } from "./PassportElementErrorFrontSide";
import { PassportElementErrorReverseSide } from "./PassportElementErrorReverseSide";
import { PassportElementErrorSelfie } from "./PassportElementErrorSelfie";
export declare type PassportElementError = PassportElementErrorDataField | PassportElementErrorFrontSide | PassportElementErrorReverseSide | PassportElementErrorSelfie | PassportElementErrorFile | PassportElementErrorFiles;
//# sourceMappingURL=PassportElementError.d.ts.map