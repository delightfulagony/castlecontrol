export default function getBot(bot: keyof typeof import("./config").BOT_TOKENS, shouldTick?: boolean): Promise<import("./bot/TelegramBot").default>;
//# sourceMappingURL=getBot.d.ts.map