declare type Basic = import("./Basic").default<"MySql">;
export default interface BasicDatabase extends Basic {
    /** The database to use by default in queries */
    database: string;
    /** the charset to use when receiving data. Recommended to be utf8mb4 */
    charset?: string;
    username: string;
    password: string;
}
export {};
//# sourceMappingURL=BasicDatabase.d.ts.map