export declare namespace ChatWars {
    type API = API.Request | API.Response;
    namespace API {
        type Response = Response.BadFormat | Response.Forbidden | Response.InvalidToken | Response.authAdditionalOperation | Response.authorizePayment | Response.createAuthCode | Response.getInfo | Response.grantAdditionalOperation | Response.grantToken | Response.guildInfo | Response.pay | Response.payout | Response.requestBasicInfo | Response.requestProfile | Response.requestStock | Response.viewCraftbook | Response.wantToBuy;
        namespace Response {
            interface BadFormat {
                action?: string;
                result: "BadFormat";
                payload: null;
            }
            interface Forbidden {
                action: string;
                result: "Forbidden";
                payload: {
                    requiredOperation: string;
                    userId: number;
                };
            }
            interface InvalidToken {
                action: string;
                result: "InvalidToken";
                payload: {
                    token: string;
                };
            }
            type createAuthCode = createAuthCode.Ok | createAuthCode.NoSuchUser | createAuthCode.BadFormat;
            namespace createAuthCode {
                interface Ok {
                    action: "createAuthCode";
                    result: "Ok";
                    payload: {
                        userId: number;
                    };
                }
                interface NoSuchUser {
                    action: "createAuthCode";
                    result: "NoSuchUser";
                    payload: null;
                }
                interface BadFormat {
                    action?: "createAuthCode";
                    result: "BadFormat";
                    payload: null;
                }
            }
            type grantToken = grantToken.Ok | grantToken.InvalidCode | grantToken.BadFormat;
            namespace grantToken {
                interface Ok {
                    action: "grantToken";
                    payload: {
                        userId: number;
                        id: string;
                        token: string;
                    };
                    result: "Ok";
                }
                interface InvalidCode {
                    action: "grantToken";
                    result: "InvalidCode";
                    payload: {
                        userId: number;
                    };
                }
                interface BadFormat {
                    action: "grantToken";
                    result: "BadFormat";
                    payload: null;
                }
            }
            type authAdditionalOperation = authAdditionalOperation.Ok;
            namespace authAdditionalOperation {
                interface Ok {
                    /**
                     * the ID of the request.
                     */
                    uuid: string;
                    action: "authAdditionalOperation";
                    result: "Ok";
                    payload: {
                        /**
                         * the operation being granted
                         */
                        operation: string;
                        /**
                         * the telegram ID of the user
                         */
                        userId: number;
                    };
                }
            }
            type grantAdditionalOperation = grantAdditionalOperation.Ok | grantAdditionalOperation.InvalidCode | grantAdditionalOperation.BadFormat;
            namespace grantAdditionalOperation {
                interface Ok {
                    action: "grantAdditionalOperation";
                    result: "Ok";
                    payload: {
                        requestId: string;
                        userId: number;
                    };
                }
                interface InvalidCode {
                    action: "grantAdditionalOperation";
                    result: "InvalidCode";
                    payload: {
                        requestId: string;
                        userId: number;
                    };
                }
                interface BadFormat {
                    action: "grantAdditionalOperation";
                    result: "BadFormat";
                    payload: null;
                }
            }
            type authorizePayment = authorizePayment.Ok;
            namespace authorizePayment {
                interface Ok {
                    uuid: string;
                    action: "authorizePayment";
                    result: "Ok";
                    payload: {
                        fee?: {
                            gold?: number;
                            pouches?: number;
                        };
                        debit?: {
                            gold?: number;
                            pouches?: number;
                        };
                        userId: number;
                    };
                }
            }
            type pay = pay.Ok;
            namespace pay {
                interface Ok {
                    uuid: string;
                    token?: string;
                    action: "pay";
                    result: "Ok";
                    payload: {
                        fee?: {
                            gold?: number;
                            pouches?: number;
                        };
                        debit: {
                            gold?: number;
                            pouches?: number;
                        };
                        userId: number;
                    };
                }
            }
            type payout = payout.Ok;
            namespace payout {
                interface Ok {
                    uuid: string;
                    action: "payout";
                    result: "Ok";
                    payload: {
                        userId: number;
                    };
                }
            }
            type getInfo = getInfo.Ok;
            namespace getInfo {
                interface Ok {
                    action: "getInfo";
                    result: "Ok";
                    payload: {
                        /** Account balance denoted in gold */
                        balance: number;
                    };
                }
            }
            type viewCraftbook = viewCraftbook.Ok;
            namespace viewCraftbook {
                interface CraftbookEntry {
                    /** the item code that is used for ingame interaction */
                    id: string;
                    name: string;
                    price: number;
                }
                interface Ok {
                    action: "viewCraftbook";
                    result: "Ok";
                    payload: {
                        alchemy?: CraftbookEntry[];
                        craft?: CraftbookEntry[];
                        userId: number;
                    };
                }
            }
            type requestProfile = requestProfile.Ok | requestProfile.InvalidToken | requestProfile.Forbidden;
            namespace requestProfile {
                interface Ok {
                    action: "requestProfile";
                    result: "Ok";
                    payload: {
                        profile: {
                            atk?: number;
                            castle?: string;
                            class?: string;
                            def?: number;
                            exp?: number;
                            gold?: number;
                            guild?: string;
                            guild_tag?: string;
                            lvl?: number;
                            mana?: number;
                            pouches?: number;
                            stamina?: number;
                            userName?: string;
                        };
                        userId: number;
                    };
                }
                interface Forbidden {
                    action: "requestProfile";
                    result: "Forbidden";
                    payload: {
                        requiredOperation: "GetUserProfile";
                        userId: number;
                    };
                }
                interface InvalidToken {
                    action: "requestProfile";
                    result: "InvalidToken";
                    payload: {
                        token: string;
                    };
                }
            }
            type requestBasicInfo = requestBasicInfo.Ok;
            namespace requestBasicInfo {
                interface Ok {
                    action: "requestBasicInfo";
                    result: "Ok";
                    payload: {
                        profile: {
                            class: string;
                            atk: number;
                            def: number;
                        };
                        userId: number;
                    };
                }
            }
            type requestStock = requestStock.Ok | requestStock.Forbidden | requestStock.InvalidToken;
            namespace requestStock {
                interface Ok {
                    action: "requestStock";
                    result: "Ok";
                    payload: {
                        /**
                         * This does not include items that are currently equipped or in the backpack.
                         */
                        stock: {
                            /**
                             * The name of the item.
                             */
                            [item: string]: number;
                        };
                        userId: number;
                    };
                }
                interface Forbidden {
                    action: "requestStock";
                    result: "Forbidden";
                    payload: {
                        requiredOperation: "GetStock";
                        userId: number;
                    };
                }
                interface InvalidToken {
                    action: "requestStock";
                    result: "InvalidToken";
                    payload: {
                        token: string;
                    };
                }
            }
            type guildInfo = guildInfo.Ok;
            namespace guildInfo {
                interface Ok {
                    action: "guildInfo";
                    result: "Ok";
                    payload: {
                        tag: string;
                        level: number;
                        castle: string;
                        glory: number;
                        members: number;
                        name: string;
                        lobby: string;
                        stock: {
                            [item: string]: number;
                        };
                        userId: number;
                    };
                }
            }
            type wantToBuy = wantToBuy.Ok | wantToBuy.Forbidden | wantToBuy.InvalidToken | wantToBuy.ProhibitedItem | wantToBuy.BadFormat;
            namespace wantToBuy {
                interface Ok {
                    action: "wantToBuy";
                    result: "Ok";
                    payload: {
                        itemName: string;
                        quantity: number;
                        userId: number;
                    };
                }
                interface Forbidden {
                    action: "wantToBuy";
                    result: "Forbidden";
                    payload: {
                        requiredOperation: "TradeTerminal";
                        userId: number;
                    };
                }
                interface InvalidToken {
                    action: "wantToBuy";
                    result: "InvalidToken";
                    payload: {
                        token: string;
                    };
                }
                interface ProhibitedItem {
                    action: "wantToBuy";
                    result: "ProhibitedItem";
                    payload: {
                        userId: number;
                        itemName?: string;
                    };
                }
                interface BadFormat {
                    action: "wantToBuy";
                    result: "BadFormat";
                    payload: {
                        userId: number;
                        itemName?: string;
                    };
                }
            }
        }
        type Request = Request.authAdditionalOperation | Request.authorizePayment | Request.createAuthCode | Request.getInfo | Request.grantAdditionalOperation | Request.grantToken | Request.guildInfo | Request.pay | Request.payout | Request.requestBasicInfo | Request.requestProfile | Request.requestStock | Request.viewCraftbook | Request.wantToBuy;
        namespace Request {
            interface authAdditionalOperation {
                token: string;
                action: "authAdditionalOperation";
                payload: {
                    operation: string;
                };
            }
            interface authorizePayment {
                token: string;
                action: "authorizePayment";
                payload: {
                    amount: {
                        pouches: number;
                    };
                    transactionId: string;
                };
            }
            interface createAuthCode {
                action: "createAuthCode";
                payload: {
                    userId: number;
                };
            }
            interface getInfo {
                action: "getInfo";
            }
            interface grantAdditionalOperation {
                token: string;
                action: "grantAdditionalOperation";
                payload: {
                    requestId: string;
                    authCode: string;
                };
            }
            interface grantToken {
                action: "grantToken";
                payload: {
                    authCode: string;
                };
            }
            interface guildInfo {
                token: string;
                action: "guildInfo";
            }
            interface pay {
                token: string;
                action: "pay";
                payload: {
                    amount: {
                        pouches: number;
                    };
                    transactionId: string;
                    confirmationCode: string;
                };
            }
            interface payout {
                token: string;
                action: "payout";
                payload: {
                    amount: {
                        pouches: number;
                    };
                    transactionId: string;
                    message: string;
                };
            }
            interface requestBasicInfo {
                token: string;
                action: "requestBasicInfo";
            }
            interface requestProfile {
                action: "requestProfile";
                token: string;
            }
            interface requestStock {
                action: "requestStock";
                token: string;
            }
            interface viewCraftbook {
                token: string;
                action: "viewCraftbook";
            }
            interface wantToBuy {
                token: string;
                action: "wantToBuy";
                payload: {
                    itemCode: string;
                    quantity: number;
                    price: number;
                    /**
                     * if true, will force purchase to be at specified price, otherwise it may
                     */
                    exactPrice: boolean;
                };
            }
        }
    }
}
//# sourceMappingURL=chatwars.d.ts.map