import TelegramBot from "../../bot/TelegramBot";
import { Message } from "../../telegram/types/Message";
export default function setupStartBot(): Promise<void>;
export declare function getBotListener(bot: TelegramBot): (msg: Message) => Promise<void>;
//# sourceMappingURL=setupStartBot.d.ts.map