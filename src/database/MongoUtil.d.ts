import { User } from "../telegram/types/User";
export declare function isBetweenRange({ min, max, }: {
    min?: number | undefined;
    max?: number | undefined;
}): (value: number) => boolean;
export declare function isStringLengthBetweenRange(max: number, min?: number): (value: string) => boolean;
export declare function isPositive(value: number): boolean;
export declare function stringLengthLessThan(length: number): (value: string) => boolean;
export declare function doMultipleChecks<T>(...checks: Array<(data: T) => boolean>): (value: T) => boolean;
export interface Parser<U> {
    errors: Array<Error | EvalError | RangeError | ReferenceError | SyntaxError | TypeError>;
    failed: boolean;
    name: string;
    value?: U;
    callback(value: any): void;
}
export declare const NaNdate: Readonly<Date>;
export declare function dateParser(this: Parser<Date>): Date;
export declare function dateParser(this: Parser<Date>, value: number | string): Date;
export declare function dateParser(this: Parser<Date>, year: number, month: number, date?: number, hours?: number, minutes?: number, seconds?: number, ms?: number): Date;
export declare function stringParser(this: Parser<string>, value: string): string;
export declare function numberParser(this: Parser<number>, value: number): number;
export declare function optionalParser(this: Parser<undefined | null>, value: undefined | null): undefined | null;
export declare function booleanParser(this: Parser<1 | 0 | boolean>, value: 1 | 0 | boolean): boolean;
export declare function userParser(this: Parser<User>, value: User): User;
export declare function addParser<T>(array: Array<Parser<T>>): (...parsers: ("string" | "number" | "boolean" | "date" | "optional" | "User" | ((this: Parser<T>, value: T) => boolean))[]) => void;
//# sourceMappingURL=MongoUtil.d.ts.map