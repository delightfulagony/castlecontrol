import { ChatPhoto } from "../telegram/types/ChatPhoto";
import { Message } from "../telegram/types/Message";
import { MetadataForInterface } from "./DatabaseTemplate";
import { FindOneAndUpdateOption, FindOneOptions, ClientSession } from "mongodb";
import { ChatMember } from "../telegram/types/ChatMember";
import { User } from "../telegram/types/User";
/**
 * This object represents a chat.
 */
export interface IChat {
    /**
     * Unique identifier for this chat. This number may be greater than 32 bits and some programming languages may have difficulty/silent defects in interpreting it. But it is smaller than 52 bits, so a signed 64 bit integer or double-precision float type are safe for storing this identifier.
     */
    id: number;
    /**
     * Type of chat, can be either “private”, “group”, “supergroup” or “channel”
     */
    type: "private" | "group" | "supergroup" | "channel";
    /**
     * Optional. Title, for supergroups, channels and group chats
     */
    title?: string;
    /**
     * Optional. Username, for private chats, supergroups and channels if available
     */
    username?: string;
    /**
     * Optional. First name of the other party in a private chat
     */
    first_name?: string;
    /**
     * Optional. Last name of the other party in a private chat
     */
    last_name?: string;
    /**
     * Optional. True if a group has ‘All Members Are Admins’ enabled.
     */
    all_members_are_administrators?: boolean;
    /**
     * Optional. Chat photo. Returned only in getChat.
     */
    photo?: ChatPhoto;
    /**
     * Optional. Description, for supergroups and channel chats. Returned only in getChat.
     */
    description?: string;
    /**
     * Optional. Chat invite link, for supergroups and channel chats. Returned only in getChat.
     */
    invite_link?: string;
    /**
     * Optional. Pinned message, for supergroups and channel chats. Returned only in getChat.
     */
    pinned_message?: Message;
    /**
     * Optional. For supergroups, name of group sticker set. Returned only in getChat.
     */
    sticker_set_name?: string;
    /**
     * Optional. True, if the bot can change the group sticker set. Returned only in getChat.
     */
    can_set_sticker_set?: boolean;
    /**
     * a parameter for welcoming new members to the chat
     */
    chat_welcome_message?: string;
    chat_welcome_back_message?: string;
    order_chat?: boolean;
    admins?: ChatMember[];
    members?: number[];
    shouldSendReminders?: boolean;
    unpinOnBattle?: boolean;
}
export declare function parseChat(data: IChat): MetadataForInterface<typeof data>;
declare const BaseClass: {
    new (data: IChat | MetadataForInterface<IChat>): {
        getCollection(): Promise<import("mongodb").Collection<IChat>>;
        _findOneAndUpdate(filter: import("mongodb").FilterQuery<IChat>, update: IChat | import("mongodb").UpdateQuery<IChat>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IChat>>): void;
        _findOneAndUpdate(filter: import("mongodb").FilterQuery<IChat>, update: IChat | import("mongodb").UpdateQuery<IChat>, options?: FindOneAndUpdateOption | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<IChat>>;
        _findOneAndUpdate(filter: import("mongodb").FilterQuery<IChat>, update: IChat | import("mongodb").UpdateQuery<IChat>, options: FindOneAndUpdateOption, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IChat>>): void;
        _findOne(filter: import("mongodb").FilterQuery<IChat>, callback: import("mongodb").MongoCallback<IChat | null>): void;
        _findOne(filter: import("mongodb").FilterQuery<IChat>, options?: FindOneOptions | undefined): Promise<IChat | null>;
        _findOne(filter: import("mongodb").FilterQuery<IChat>, options: FindOneOptions, callback: import("mongodb").MongoCallback<IChat | null>): void;
        _findOneAndDelete(filter: import("mongodb").FilterQuery<IChat>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IChat>>): void;
        _findOneAndDelete(filter: import("mongodb").FilterQuery<IChat>, options?: {
            projection?: Object | undefined;
            sort?: Object | undefined;
            maxTimeMS?: number | undefined;
            session?: ClientSession | undefined;
        } | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<IChat>>;
        _findOneAndDelete(filter: import("mongodb").FilterQuery<IChat>, options: {
            projection?: Object | undefined;
            sort?: Object | undefined;
            maxTimeMS?: number | undefined;
            session?: ClientSession | undefined;
        }, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IChat>>): void;
        toSimpleObject(): Partial<IChat>;
    };
    getCollection(): Promise<import("mongodb").Collection<IChat>>;
    _findOneAndUpdate(filter: import("mongodb").FilterQuery<IChat>, update: IChat | import("mongodb").UpdateQuery<IChat>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IChat>>): void;
    _findOneAndUpdate(filter: import("mongodb").FilterQuery<IChat>, update: IChat | import("mongodb").UpdateQuery<IChat>, options?: FindOneAndUpdateOption | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<IChat>>;
    _findOneAndUpdate(filter: import("mongodb").FilterQuery<IChat>, update: IChat | import("mongodb").UpdateQuery<IChat>, options: FindOneAndUpdateOption, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IChat>>): void;
    _findOne(filter: import("mongodb").FilterQuery<IChat>, callback: import("mongodb").MongoCallback<IChat | null>): void;
    _findOne(filter: import("mongodb").FilterQuery<IChat>, options?: FindOneOptions | undefined): Promise<IChat | null>;
    _findOne(filter: import("mongodb").FilterQuery<IChat>, options: FindOneOptions, callback: import("mongodb").MongoCallback<IChat | null>): void;
    _findOneAndDelete(filter: import("mongodb").FilterQuery<IChat>, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IChat>>): void;
    _findOneAndDelete(filter: import("mongodb").FilterQuery<IChat>, options?: {
        projection?: Object | undefined;
        sort?: Object | undefined;
        maxTimeMS?: number | undefined;
        session?: ClientSession | undefined;
    } | undefined): Promise<import("mongodb").FindAndModifyWriteOpResultObject<IChat>>;
    _findOneAndDelete(filter: import("mongodb").FilterQuery<IChat>, options: {
        projection?: Object | undefined;
        sort?: Object | undefined;
        maxTimeMS?: number | undefined;
        session?: ClientSession | undefined;
    }, callback: import("mongodb").MongoCallback<import("mongodb").FindAndModifyWriteOpResultObject<IChat>>): void;
};
export default class TelegramChat extends BaseClass implements IChat {
    static getAllChats(): AsyncIterableIterator<TelegramChat | null>;
    static findOne(chat: number, options?: FindOneOptions): Promise<IChat | null>;
    id: number;
    type: "private" | "group" | "supergroup" | "channel";
    title?: string;
    username?: string;
    first_name?: string;
    last_name?: string;
    all_members_are_administrators?: boolean;
    photo?: ChatPhoto;
    description?: string;
    invite_link?: string;
    pinned_message?: Message;
    sticker_set_name?: string;
    can_set_sticker_set?: boolean;
    chat_welcome_back_message?: string;
    chat_welcome_message?: string;
    order_chat?: boolean;
    admins?: ChatMember[];
    members?: number[];
    _lastRefreshed?: Date;
    _lastGetChat?: Date;
    shouldSendReminders?: boolean;
    unpinOnBattle?: boolean;
    syncChatState(): Promise<void>;
    addChatMember(user: number | User | TelegramChat): void;
    runNormalCheck(): Promise<void>;
    findOneAndUpdate(options?: FindOneAndUpdateOption): Promise<import("mongodb").FindAndModifyWriteOpResultObject<IChat>>;
    findOne(options?: FindOneOptions): Promise<IChat | null>;
    findOneAndDelete(options?: {
        projection?: Object;
        sort?: Object;
        maxTimeMS?: number;
        session?: ClientSession;
    }): Promise<import("mongodb").FindAndModifyWriteOpResultObject<IChat>>;
}
export {};
//# sourceMappingURL=TelegramChat.d.ts.map