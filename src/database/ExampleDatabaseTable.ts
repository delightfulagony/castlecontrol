/*
This is an example table provided that will not be used in production.
The purpose of this is to simplify the process of adding new tables in your own contributions.
If you require an additional property added to a pre-existing table, get in contact with me so I can add it to the parser.
Do note, that I will not provide actual validation of the additional property, and it is up to you to do that.
Despite this, I do encourage you to use pre-existing tables where possible, as it could help you lower queries made.
*/
import createDataClass, { MetadataForInterface } from "./DatabaseTemplate";
import { stringParser, numberParser, Parser, optionalParser } from "./MongoUtil";

export interface IExampleDatabaseTable {
    hello?: string;
    world: string;
    exclamationPoints?: number;
}
/**
 * parses the data, and returns a clean version of it.
 * @param data the data to validate
 */
export function parseExample(data: IExampleDatabaseTable): MetadataForInterface<typeof data> {
    type T = MetadataForInterface<typeof data>;
    const errorList: string[] = [];
    const result: T = {
        _refreshDate: new Date(),
        world: "N/A",
    } as T;
    const keys = Object.keys(data) as Array<keyof typeof data>;
    const otherKeys = Object.keys(result).filter((value) => { return value !== "_refreshDate"; }) as Array<keyof T>;
    for (const i of otherKeys) {
        let isThere = false;
        lol: for (const j of keys) {
            if (j === i) {
                isThere = true;
                break lol;
            }
        }
        if (!isThere) {
            errorList.push(`${i} is not present in given data`);
        }
    }
    for (const i of keys) {
        const value = data[i];
        const parsersToUse: Array<Parser<T[keyof T]>> = [];
        thing: switch (i) {
            case "hello":
                parsersToUse.push({
                    callback: optionalParser,
                    errors: [],
                    failed: false,
                    name: "optional",
                });
            case "world":
                parsersToUse.push({
                    callback: stringParser,
                    errors: [],
                    failed: false,
                    name: "string",
                });
                break thing;
            case "exclamationPoints":
                parsersToUse.push({
                    callback: optionalParser,
                    errors: [],
                    failed: false,
                    name: "optional",
                }, {
                    callback: numberParser,
                    errors: [],
                    failed: false,
                    name: "number",
                });
            default:
                result[i] = data[i];
        }
        let testPassed: boolean | null = null;
        const dataTypes: string[] = [];
        let newValue = value;
        lol: for (const test of parsersToUse) {
            test.callback(value);
            if (testPassed === null && test.failed) {
                testPassed = false;
            } else if (!test.failed) {
                testPassed = true;
                newValue = test.value as typeof newValue;
                break lol;
            }
            dataTypes.push(test.name);
        }
        if (testPassed || testPassed === null) {
            result[i] = newValue;
        } else {
            if (dataTypes.length > 1) {
                dataTypes[dataTypes.length - 1] = "or " + dataTypes[dataTypes.length - 1];
            }
            errorList.push(`${i} was not a ${dataTypes.join(", ")}.`);
        }
    }
    if (errorList.length > 0) {
        throw new Error(`Object failed verification:\n${errorList.join("\n")}`);
    }
    return result;
}
const BaseClass = createDataClass<IExampleDatabaseTable>("example", parseExample);
export default class ExampleDatabaseTable extends BaseClass implements IExampleDatabaseTable {
    public hello?: string | undefined;
    public world!: string;
    public exclamationPoints?: number | undefined;
}
