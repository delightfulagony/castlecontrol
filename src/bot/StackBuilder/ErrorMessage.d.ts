declare const imports: {
    readonly StackBuilder: typeof import("../StackBuilder").default;
};
export declare const error: unique symbol;
export declare type error = typeof error;
export default class ErrorMessage extends imports.StackBuilder {
    static stackTraceLimit: number;
    static areMessagesPending(): boolean;
    static getPendingMessage(): ErrorMessage | null;
    private static pendingMessages;
    constructor(...args: ConstructorParameters<typeof imports.StackBuilder>);
    queueForLog(): void;
}
export {};
//# sourceMappingURL=ErrorMessage.d.ts.map