/// <reference types="node" />
declare const imports: {
    readonly LOGGING_CHANNEL: typeof import("../config").LOGGING_CHANNEL;
    readonly getBot: typeof import("../getBot").default;
    readonly wait: typeof import("../util/wait").default;
    readonly ErrorMessage: typeof import("./StackBuilder/ErrorMessage").default;
    readonly FatalMessage: typeof import("./StackBuilder/FatalMessage").default;
    readonly LogMessage: typeof import("./StackBuilder/LogMessage").default;
    readonly VerboseMessage: typeof import("./StackBuilder/VerboseMessage").default;
    readonly WarnMessage: typeof import("./StackBuilder/WarnMessage").default;
    readonly ReadableMessage: typeof import("./StackBuilder/ReadableMessage").default;
    readonly error: typeof import("./StackBuilder/ErrorMessage").error;
    readonly fatal: typeof import("./StackBuilder/FatalMessage").fatal;
    readonly log: typeof import("./StackBuilder/LogMessage").log;
    readonly verbose: typeof import("./StackBuilder/VerboseMessage").verbose;
    readonly warn: typeof import("./StackBuilder/WarnMessage").warn;
    readonly readable: typeof import("./StackBuilder/ReadableMessage").readable;
};
export default class StackBuilder extends Error {
    static indent: string;
    static getLocation(trace: NodeJS.CallSite): string;
    static prepareStackTrace(error: Error, stackTraces: NodeJS.CallSite[]): string;
    static getMessageToLog(): InfoMessage | null;
    date: Date;
    constructor(...args: Parameters<typeof Error>);
}
export declare type InfoMessage = typeof imports.ReadableMessage.prototype | typeof imports.VerboseMessage.prototype | typeof imports.LogMessage.prototype | typeof imports.WarnMessage.prototype | typeof imports.ErrorMessage.prototype | typeof imports.FatalMessage.prototype;
export declare type InfoMessageConstructor = typeof imports.ReadableMessage | typeof imports.VerboseMessage | typeof imports.LogMessage | typeof imports.WarnMessage | typeof imports.ErrorMessage | typeof imports.FatalMessage;
export declare type LOG_LEVEL_NAMES = "readable" | "verbose" | "log" | "warn" | "error" | "fatal";
export declare type LOG_LEVELS = typeof imports.readable | typeof imports.verbose | typeof imports.log | typeof imports.warn | typeof imports.error | typeof imports.fatal;
export {};
//# sourceMappingURL=StackBuilder.d.ts.map