export declare const FORCEFUL_ALLOW_EVENT: unique symbol;
export declare const FORCEFUL_DENY_EVENT: unique symbol;
export declare const DENY_EVENT: unique symbol;
export declare const ALLOW_EVENT: unique symbol;
export declare type possible_event_results = typeof FORCEFUL_ALLOW_EVENT | typeof FORCEFUL_DENY_EVENT | typeof ALLOW_EVENT | typeof DENY_EVENT;
//# sourceMappingURL=EventFilters.d.ts.map