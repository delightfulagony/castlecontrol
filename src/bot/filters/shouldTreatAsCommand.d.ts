import { Message } from "../../telegram/types/Message";
import { Update } from "../../telegram/types/Update";
export default function shouldTreatAsCommand(data?: Update): import("../EventFilters").possible_event_results;
export default function shouldTreatAsCommand(data?: Message): import("../EventFilters").possible_event_results;
//# sourceMappingURL=shouldTreatAsCommand.d.ts.map