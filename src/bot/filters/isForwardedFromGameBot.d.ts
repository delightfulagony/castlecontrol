import { Message } from "../../telegram/types/Message";
import { Update } from "../../telegram/types/Update";
export default function isForwardedFromGameBot(data?: Update): import("../EventFilters").possible_event_results;
export default function isForwardedFromGameBot(data?: Message): import("../EventFilters").possible_event_results;
//# sourceMappingURL=isForwardedFromGameBot.d.ts.map