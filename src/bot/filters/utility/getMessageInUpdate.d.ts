import { Update } from "../../../telegram/types/Update";
import { Message } from "../../../telegram/types/Message";
export default function getMessageInUpdate(update: Update): Message | undefined;
//# sourceMappingURL=getMessageInUpdate.d.ts.map