import { Update } from "../../../telegram/types/Update";
import { ShippingQuery } from "../../../telegram/types/ShippingQuery";
export default function getShippingQueryInUpdate(update: Update): ShippingQuery | undefined;
//# sourceMappingURL=getShippingQueryInUpdate.d.ts.map