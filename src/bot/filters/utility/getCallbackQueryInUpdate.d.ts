import { Update } from "../../../telegram/types/Update";
import { CallbackQuery } from "../../../telegram/types/CallbackQuery";
export default function getCallbackQueryInUpdate(update: Update): CallbackQuery | undefined;
//# sourceMappingURL=getCallbackQueryInUpdate.d.ts.map