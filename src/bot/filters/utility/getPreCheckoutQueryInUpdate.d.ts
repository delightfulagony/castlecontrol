import { Update } from "../../../telegram/types/Update";
import { PreCheckoutQuery } from "../../../telegram/types/PreCheckoutQuery";
export default function getPreCheckoutQueryInUpdate(update: Update): PreCheckoutQuery | undefined;
//# sourceMappingURL=getPreCheckoutQueryInUpdate.d.ts.map