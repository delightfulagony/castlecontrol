import EventListener from "./EventListener";
declare type modifierThing<T> = import("./EventListener").modifierThing<T>;
declare type eventTypes = import("./EventListener").eventTypes;
declare type onEvents<T> = Array<eventTypes | modifierThing<T>>;
declare type callbackType<T> = import("./EventListener").callbackType<T>;
declare type onCallback<T> = (data: callbackType<T>) => void | Promise<void>;
declare type UpdateConstructor = typeof import("../telegram/types/Update").Update;
declare type Update = UpdateConstructor["prototype"];
declare const primitive: unique symbol;
/**
 * The event emitter for updates to the bot.
 * This will handle distribution of events.
 * The reason checks are done from the distribution area is because it allows the amount of code executed to be lowered.
 * For asynchronous checks that access a database, it is very important to not execute those checks more than neccessary.
 */
export default class BotEventEmitter {
    private [primitive];
    /**
     * This will listen for an event
     * @param cbArg the datatype you want to receive when the update is called.
     * @param cb the callback to execute when the event meets the requirements
     * @param events conditions for the event to execute
     */
    on<L extends import("../types/bot/TypeSwappers").CallbackConstructorTypes>(cbArg: L, cb: onCallback<L["prototype"]>, ...events: onEvents<Update>): EventListener<L>;
    /**
     * This will listen for an event, and deactivate after running once.
     * @param cbArg the datatype you want to receive when the update is called.
     * @param cb the callback to execute when the event meets the requirements
     * @param events conditions for the event to execute
     */
    once<L extends import("../types/bot/TypeSwappers").CallbackConstructorTypes>(cbArg: L, cb: onCallback<L["prototype"]>, ...events: onEvents<L["prototype"]>): EventListener<L>;
    /**
     * Take in an update from telegram.
     * It will then work out what events may apply to it.
     * It will then go through additional parameters the event may set
     * If the update meets all the requirements of the event, it will execute the event.
     *
     * If an additional parameter is used multiple times,
     * it will intelligently avoid repeating the execution of that conditional.
     * @param update the update to push to the listeners
     */
    emit(update: Update): Promise<void>;
    /**
     * allows the list of event listeners to be iterated through.
     */
    [Symbol.iterator](): IterableIterator<EventListener<any>>;
}
export {};
//# sourceMappingURL=BotEventEmitter.d.ts.map