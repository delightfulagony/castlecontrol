import { Chat } from "../telegram/types/Chat";
export default function checkChatOwnership(chat: Chat | number): Promise<boolean>;
//# sourceMappingURL=checkChatOwnership.d.ts.map