import { User } from "../telegram/types/User";
import { BotAdminType } from "./BotAdminType";
export default function doesUserHaveRightsToAdministrativeStatus(userId: number, status: BotAdminType): boolean;
export default function doesUserHaveRightsToAdministrativeStatus(user: User, status: BotAdminType): boolean;
//# sourceMappingURL=doesUserHaveRightsToAdministrativeStatus.d.ts.map